<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="http://croogo.org">Croogo</a>
      <div class="nav-collapse">
        <ul class="nav">
          <li><a href="http://blog.croogo.org/">Blog</a></li>
          <li><a href="http://docs.croogo.org/">Docs</a></li>
          <li><a href="http://wiki.croogo.org/extensions">Extensions</a></li>
          <li><a href="https://github.com/croogo/croogo/downloads">Download</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
</div>
