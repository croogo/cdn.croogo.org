<h3>Useful links</h3>

<ul>
  <li><a href="http://wiki.croogo.org">Wiki</a></li>
  <li><a href="http://blog.croogo.org">Blog</a></li>
  <li><a href="http://groups.google.com/group/croogo">Forum</a></li>
  <li><a href="http://croogo.lighthouseapp.com/">Issue Tracker</a></li>
  <li><a href="http://github.com/croogo/croogo/croogo">Source</a></li>
</ul>